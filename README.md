# FormationGPU

Formation pour mettre en place l'intégration continue depuis la plateforme Inria ( https://ci.inria.fr ) en compilant sur la plateforme PlaFRIM  ( https://www.plafrim.fr ) des projets pour GPU

# Liens externes

## PlaFRIM :
- contact : contact@plafrim.fr
- doumentation : https://www.plafrim.fr/fr/la-plateforme/documentation-logicielle/

## Intégration continue :
- documentation : https://wiki.inria.fr/ciportal/Main_Page
- contact : ci-community@inria.fr
 
## Exemple CUDA :

Sample code to do array multiplication on a GPU with CUDA taken from the tutorial found here: https://devblogs.nvidia.com/parallelforall/even-easier-introduction-cuda/

Compile with:
```bash
module load compiler/cuda/7.5/toolkit/7.5.18
module load compiler/gcc/4.8.4
nvcc add.cu -o add_cuda
```

## Exemple avec numba :

Sample code to use numba : https://weeraman.com/put-that-gpu-to-good-use-with-python-e5a437168c01