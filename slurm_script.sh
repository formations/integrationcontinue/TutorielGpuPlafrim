#!/bin/bash
#SBATCH -p court_sirocco
#SBATCH -o cuda%j.out
#SBATCH -e cuda%j.err
#
#SBATCH --gres=gpu:2
#SBATCH -N2
#SBATCH --time=0:05:00

module load compiler/cuda/8.0/toolkit/8.0.61

export NUMBAPRO_NVVM="/cm/shared/apps/cuda80/toolkit/8.0.61/nvvm/lib64/libnvvm.so"
export NUMBAPRO_LIBDEVICE="/cm/shared/apps/cuda80/toolkit/8.0.61/nvvm/libdevice/"
srun python numpy_pow.py

